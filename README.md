## Как запустить
Для начала нужно скачать образ (image) Mysql из докер хаба, можно через Docker Desktop или через команду
```
docker pull mysql
```
Далее можно создать volume, который сохранит данные даже если удалить контейнер, но в данном случае пропустим этот момент
### Запуск контейнера mysql
```
docker run --name mysql1 -p 3307:3306 -e MYSQL_ROOT_PASSWORD=mysql -e MYSQL_DATABASE=testdb1 mysql
```
### Создать image приложения
```
docker build -t app-image .
```
### Запуск приложения
```
docker run --name application-1 -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=mysql -e SPRING_DATASOURCE_URL=jdbc:mysql://host.docker.internal:3307/testdb1 -p 80:80 app-image
```
### Проверить на каком ip поднялось приложение
```
docker inspect application-1 | grep "IPAddress"
```
### Покажет все контейнеры, заодно порты на каком развернулись контейнеры
```
docker ps
```
### Оставновить контейнеры после окончания работы с приложением
```
docker stop application-1
```
```
docker stop mysql1
```