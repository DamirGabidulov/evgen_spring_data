package ru.spring.dto;

import lombok.Builder;
import lombok.Data;
import ru.spring.model.Role;

import java.time.LocalDate;

@Data
@Builder
public class UserResponse {
    private Integer id;
    private String name;
    private String username;
    private Role role;
}
