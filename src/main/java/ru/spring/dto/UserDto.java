package ru.spring.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ru.spring.model.Event;
import ru.spring.model.Status;
import ru.spring.model.User;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Integer id;
    private String name;
    @JsonIgnore
    private List<Event> events;
    private Status status;

    public static UserDto from(User user){
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .events(user.getEvents())
                .status(user.getStatus())
                .build();
    }

    public static List<UserDto> from(List<User> users){
        return users.stream().map(UserDto::from).collect(Collectors.toList());
    }
}
