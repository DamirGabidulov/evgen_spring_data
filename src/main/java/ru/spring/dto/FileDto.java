package ru.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.spring.model.File;
import ru.spring.model.Status;

import java.util.List;
import java.util.stream.Collectors;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Integer id;
    private String location;
    private String name;
    private Status status;

    public static FileDto from(File file){
        return FileDto.builder()
                .id(file.getId())
                .location(file.getLocation())
                .name(file.getName())
                .status(file.getStatus())
                .build();
    }

    public static List<FileDto> from(List<File> files){
        return files.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
