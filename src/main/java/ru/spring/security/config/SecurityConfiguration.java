package ru.spring.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.spring.repository.UserRepository;
import ru.spring.security.detail.UserDetailsServiceImpl;
import ru.spring.security.filter.TokenAuthenticationFilter;
import ru.spring.security.filter.TokenAuthorizationFilter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_URL = "/api/v1/login";
    public static final String USERS_URL = "/api/v1/users";
    public static final String EVENTS_URL = "/api/v1/events";
    public static final String FILES_URL = "/api/v1/files";

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private UserRepository userRepository;


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        TokenAuthenticationFilter tokenAuthenticationFilter = new TokenAuthenticationFilter(authenticationManagerBean(),
                objectMapper, userRepository);
        tokenAuthenticationFilter.setFilterProcessesUrl(LOGIN_URL);

        //нам здесь это не нужно, отключили защиту
        http.csrf().disable();
        //отключили сессию
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //положили наш фильтр
        http.addFilter(tokenAuthenticationFilter);
        //положили наш второй фильтр. UsernamePasswordAuthenticationFilter - выполняется перед этим фильтром
        http.addFilterBefore(new TokenAuthorizationFilter(objectMapper, userDetailsService),
                UsernamePasswordAuthenticationFilter.class);

        http.authorizeRequests()
                .antMatchers(LOGIN_URL).permitAll()
                .antMatchers(HttpMethod.POST, USERS_URL + "/signUp").permitAll()
                .antMatchers(HttpMethod.PUT, USERS_URL + "/**").hasAuthority("USER")
                .antMatchers(HttpMethod.DELETE, USERS_URL + "/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, USERS_URL + "/*").hasAuthority("USER")
                .antMatchers(HttpMethod.GET, USERS_URL).hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, EVENTS_URL).hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, FILES_URL + "/**").hasAuthority("USER")
                .antMatchers(HttpMethod.PUT, FILES_URL + "/**").hasAuthority("USER")
                .antMatchers(HttpMethod.DELETE, FILES_URL + "/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, FILES_URL + "/*").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, FILES_URL).hasAuthority("ADMIN");
    }
}
