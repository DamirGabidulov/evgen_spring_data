package ru.spring.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Event> events;

    @Column
    private Status status;

    private String token;

    @Column(unique = true)
    private String username;

    private String password;
    @Enumerated(value = EnumType.STRING)
    private Role role;
}
