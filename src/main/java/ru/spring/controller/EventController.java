package ru.spring.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spring.dto.EventDto;
import ru.spring.model.User;
import ru.spring.repository.EventRepository;
import ru.spring.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/events")
public class EventController {

    private final UserRepository userRepository;

    @GetMapping
    public ResponseEntity<List<EventDto>> findAll(@RequestHeader("userId") Integer userId){
        User user = userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        List<EventDto> eventDtoList = EventDto.from(user.getEvents());
        if (eventDtoList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return ResponseEntity.ok().body(eventDtoList);
        }
    }
}
