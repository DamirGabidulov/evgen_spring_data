package ru.spring.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.spring.dto.EventDto;
import ru.spring.model.Event;
import ru.spring.repository.EventRepository;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;

    public EventDto save(EventDto eventDto){
        Event event = Event.builder()
                .user(eventDto.getUser())
                .file(eventDto.getFile())
                .status(eventDto.getStatus())
                .build();
        return EventDto.from(eventRepository.save(event));
    }

}
