package ru.spring.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.EventDto;
import ru.spring.dto.UserDto;
import ru.spring.model.File;
import ru.spring.model.Status;
import ru.spring.repository.FileRepository;
import ru.spring.util.FileUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static ru.spring.dto.FileDto.from;
import static ru.spring.service.TestUtils.*;

@ExtendWith(MockitoExtension.class)
class FileServiceTest {

    @Mock
    FileRepository fileRepository;
    @Mock
    UserService userService;
    @Mock
    EventService eventService;
    @Mock
    MultipartFile multipartFile;
    @Mock
    S3Service s3Service;
    @InjectMocks
    FileService fileService;
    public static final String ORIGINAL_FILE_NAME = "new hm.txt";

    @BeforeEach
    void setUp() {
        lenient().when(fileRepository.findAll()).thenReturn(getFiles());
        lenient().when(fileRepository.findById(1)).thenReturn(Optional.ofNullable(getFiles().get(0)));
        lenient().when(fileRepository.findById(4)).thenReturn(Optional.ofNullable(getFiles().get(3)));
        lenient().when(userService.getById(2)).thenReturn(UserDto.from(getUsers().get(1)));
        lenient().when(multipartFile.getOriginalFilename()).thenReturn(ORIGINAL_FILE_NAME);
        lenient().when(fileRepository.save(File.builder()
                .name("new hm.txt")
                .location("id2/new hm.txt")
                .status(Status.ACTIVE)
                .build())).thenReturn(getFiles().get(3));
        lenient().when(fileRepository.save(File.builder()
                .id(4)
                .name("cat.png")
                .location("id2/cat.png")
                .status(Status.ACTIVE)
                .build())).thenReturn(getUpdatedFile());
    }

    @Test
    void findAll() {
        assertEquals(getFiles(), fileService.findAll());
    }

    @Test
    void save() {
        java.io.File physicalFile = new java.io.File("/" + ORIGINAL_FILE_NAME);
        try (MockedStatic<FileUtil> fileUtil = Mockito.mockStatic(FileUtil.class)) {
            fileUtil.when((MockedStatic.Verification) FileUtil.multipartToFile(multipartFile, ORIGINAL_FILE_NAME))
                    .thenReturn(physicalFile);
            lenient().doNothing().when(s3Service).putS3Object("", physicalFile);
            assertEquals(from(getFiles().get(3)), fileService.save(multipartFile, 2));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = 1)
    void getById(Integer id) {
        assertEquals(from(getFiles().get(0)), fileService.getById(id));
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> fileService.getById(100));
        assertEquals("File with this id doesn't exist", illegalArgumentException.getMessage());
    }

    @Test
    void update() {
        java.io.File physicalFile = new java.io.File("/" + ORIGINAL_FILE_NAME);
        try (MockedStatic<FileUtil> fileUtil = Mockito.mockStatic(FileUtil.class)) {
            MultipartFile multipartFile = getMultipartFile();
            fileUtil.when((MockedStatic.Verification) FileUtil.multipartToFile(multipartFile, ORIGINAL_FILE_NAME))
                    .thenReturn(physicalFile);
            lenient().doNothing().when(s3Service).putS3Object("", physicalFile);
            lenient().doNothing().when(s3Service).deleteS3Object("");
            assertEquals(from(getUpdatedFile()), fileService.update(4, multipartFile, 2));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void delete() {
        File file = fileRepository.findById(4).get();
        fileService.delete(file.getId(), 2);
        assertEquals(file.getStatus(), Status.DELETED);
    }
}