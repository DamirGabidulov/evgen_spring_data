package ru.spring.service;

import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.SignUpForm;
import ru.spring.dto.UserResponse;
import ru.spring.model.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TestUtils {

    private final PasswordEncoder passwordEncoder;

    public static final String USER_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2Iiwicm9sZSI6IlVTRVIiLCJleHAiOjE2ODUxMjMxNzUsInVzZXJuYW1lIjoiaUFtVXNlciIsInN0YXR1cyI6IkFDVElWRSJ9.Mx8b2aoFx-8vUY5GuAd9U8FGIssK5GMUM1G2ROP7qnw";
    public static final String ADMIN_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0Iiwicm9sZSI6IkFETUlOIiwiZXhwIjoxNjg1MTIzMDk1LCJ1c2VybmFtZSI6ImlBbUFkbWluIiwic3RhdHVzIjoiQUNUSVZFIn0.6yDjwskL-Y-Kktx2MZM-1_JVDugTI-_i9CzBb1udYUE";
    protected static final String USER_PASSWORD = "$2a$10$dSpNYu0fEP5D9UDeNmQg0uzn38mcfBJy/gUzte41xvE1sNKJOYPN6";
    protected static final String ADMIN_PASSWORD = "$2a$10$qnAAHnJevjuG6IgwMZ9MCeYyM0w5vgmi/lMcF0fY6b8EIKOoIZzcq";
    protected static final String FILE_NAME = "cat.png";

    public TestUtils(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    public static List<User> getUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "first", Arrays.asList(getEvents().get(0), getEvents().get(1)), Status.ACTIVE, USER_TOKEN, "iAmUser", USER_PASSWORD, Role.USER));
        users.add(new User(2, "second", Arrays.asList(getEvents().get(2), getEvents().get(3)), Status.ACTIVE, ADMIN_TOKEN, "iAmAdmin", ADMIN_PASSWORD, Role.ADMIN));
        return users;
    }

    public static List<Event> getEvents() {
        List<Event> events = new ArrayList<>();
        events.add(new Event(1, new User(1, "first", null, Status.ACTIVE, USER_TOKEN, "iAmUser", USER_PASSWORD, Role.USER), getFiles().get(0), Status.ACTIVE));
        events.add(new Event(2, new User(1, "first", null, Status.ACTIVE, USER_TOKEN, "iAmUser", USER_PASSWORD, Role.USER), getFiles().get(1), Status.ACTIVE));
        events.add(new Event(3, new User(2, "second", null, Status.ACTIVE, ADMIN_TOKEN, "iAmAdmin", ADMIN_PASSWORD, Role.ADMIN), getFiles().get(2), Status.ACTIVE));
        events.add(new Event(4, new User(2, "second", null, Status.ACTIVE, ADMIN_TOKEN, "iAmAdmin", ADMIN_PASSWORD, Role.ADMIN), getFiles().get(3), Status.ACTIVE));
        return events;
    }

    public static List<File> getFiles() {
        List<File> files = new LinkedList<>();
        files.add(new File(1, "id1/cat.png", "cat.png", Status.ACTIVE));
        files.add(new File(2, "id1/bojack.jpg", "bojack.jpg", Status.ACTIVE));
        files.add(new File(3, "id2/hm.txt", "hm.txt", Status.ACTIVE));
        files.add(new File(4, "id2/new hm.txt", "new hm.txt", Status.ACTIVE));
        return files;
    }

    public static SignUpForm getForm() {
        return SignUpForm.builder()
                .name("first")
                .username("iAmUser")
                .password(USER_PASSWORD)
                .role(Role.USER)
                .build();
    }

    public static User fromSignUpForm(SignUpForm form, Integer userId) {
        return User.builder()
                .id(userId)
                .name(form.getName())
                .username(form.getUsername())
                .token(USER_TOKEN)
                .password(form.getPassword())
                .status(Status.ACTIVE)
                .role(form.getRole())
                .build();
    }

    public static UserResponse fromUser(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .username(user.getUsername())
                .role(user.getRole())
                .build();
    }

    public static File getUpdatedFile(){
        return File.builder()
                .id(4)
                .name(FILE_NAME)
                .location("id2/" + FILE_NAME)
                .status(Status.ACTIVE)
                .build();
    }

    public static MultipartFile getMultipartFile() throws IOException {
        String data = "Hello, world!";
        byte[] byteArray = data.getBytes(StandardCharsets.UTF_8);
        return new MockMultipartFile(FILE_NAME, FILE_NAME, String.valueOf(MediaType.APPLICATION_OCTET_STREAM), new ByteArrayInputStream(byteArray));
    }
}
