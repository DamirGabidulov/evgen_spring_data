package ru.spring.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.SignUpForm;
import ru.spring.dto.UserDto;
import ru.spring.dto.UserResponse;
import ru.spring.model.Role;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static ru.spring.service.TestUtils.*;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    public PasswordEncoder passwordEncoder;

    @InjectMocks
    UserService userService;

    @BeforeEach
    void setUp() {
        lenient().when(userRepository.findAllByStatusEquals(Status.ACTIVE)).thenReturn(TestUtils.getUsers());
        lenient().when(userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, 1)).thenReturn(Optional.of(TestUtils.getUsers().get(0)));
        lenient().when(userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, 100)).thenReturn(Optional.empty());
        lenient().when(userRepository.findByUsername("iAmAdmin")).thenReturn(Optional.of(getUsers().get(1)));
        lenient().when(userRepository.save(TestUtils.fromSignUpForm(getForm(), getUsers().get(0).getId()))).thenReturn(getUsers().get(0));
        lenient().when(userRepository.save(User.builder()
                        .id(getUsers().get(0).getId())
                        .name(getUsers().get(0).getName())
                        .events(getUsers().get(0).getEvents())
                        .status(getUsers().get(0).getStatus())
                        .build()))
                .thenReturn(getUsers().get(0));
        lenient().when(userRepository.save(User.builder()
                        .id(13)
                        .name("deleted")
                        .username("IAmDeleted")
                        .status(Status.ACTIVE)
                        .build()))
                .thenReturn(User.builder()
                        .id(13)
                        .name("deleted")
                        .username("IAmDeleted")
                        .status(Status.DELETED)
                        .build());


        this.passwordEncoder = new BCryptPasswordEncoder();
        userService = new UserService(userRepository, passwordEncoder);
    }

    @Test
    void save() {
        UserResponse userResponse = userService.save(getForm());
        //нужно ставить id, иначе выдает null
        userResponse.setId(getUsers().get(0).getId());
        assertEquals(TestUtils.fromUser(getUsers().get(0)), userResponse);
        SignUpForm form = SignUpForm.builder()
                .username("iAmAdmin")
                .password(ADMIN_PASSWORD)
                .build();
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> userService.save(form));
        assertEquals("Пользователь с таким username уже зарегистрирован!", illegalArgumentException.getMessage());
        //проверка на проставление роли
        SignUpForm formWithoutRole = SignUpForm.builder()
                .password(USER_PASSWORD)
                .build();
        assertEquals(Role.USER, userService.save(formWithoutRole).getRole());
    }

    @Test
    void isUserExist() {
        assertTrue(userService.isUserExist(1));
    }

    @Test
    void getById() {
        assertEquals(UserDto.from(getUsers().get(0)), userService.getById(1));
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> userService.getById(100));
        assertEquals("User with this id doesn't exist", illegalArgumentException.getMessage());
    }

    @Test
    void findAll() {
        assertEquals(UserDto.from(getUsers()), userService.findAll());
    }

    @Test
    void update() {
        assertEquals(UserDto.from(getUsers().get(0)), userService.update(1, UserDto.from(getUsers().get(0))));
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> userService.update(100,UserDto.from(getUsers().get(0))));
        assertEquals("User with this id doesn't exist", illegalArgumentException.getMessage());
    }

    @Test
    void isFileOwnedByUser() {
        //потому что такие данные положили в setUp()
        assertTrue(userService.isFileOwnedByUser(1, 1));
        assertFalse(userService.isFileOwnedByUser(1, 100));
        assertFalse(userService.isFileOwnedByUser(100, 1));
    }

    @Test
    void isFileWithThisNameAlreadyExist() {
        try {
            //заполнил не файлом а текстом, на суть теста влияет только название файла
            String data = "Hello, world!";
            byte[] byteArray = data.getBytes(StandardCharsets.UTF_8);
            MultipartFile multipartFile = new MockMultipartFile("cat.png", "cat.png", String.valueOf(MediaType.APPLICATION_OCTET_STREAM), new ByteArrayInputStream(byteArray));
            MultipartFile wrongMultipartFile = new MockMultipartFile("wrong name.png", "wrong name.png", String.valueOf(MediaType.APPLICATION_OCTET_STREAM), new ByteArrayInputStream(byteArray));
            assertTrue(userService.isFileWithThisNameAlreadyExist(multipartFile, 1));
            assertFalse(userService.isFileWithThisNameAlreadyExist(wrongMultipartFile, 1));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void delete() {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, 1).get();
        //обновление статуса
        userService.delete(1);
        assertEquals(user.getStatus(),Status.DELETED);
    }
}