plugins {
	java
	id("org.springframework.boot") version "2.6.0"
	id("io.spring.dependency-management") version "1.1.0"
}

group = "ru.example"
version = "0.1"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

//base {
//	archivesBaseName = "kgax"
//}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-data-rest")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("mysql:mysql-connector-java:8.0.28")
	implementation("com.amazonaws:aws-java-sdk:1.12.52")
	implementation("javax.validation:validation-api:2.0.1.Final")
	implementation("com.auth0:java-jwt:3.15.0")
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
	testImplementation("org.mockito:mockito-inline:5.2.0")


	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}
