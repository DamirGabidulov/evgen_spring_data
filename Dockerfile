FROM gradle:7.6.1-jdk11-alpine as GRADLE_BUILD
#--chown=gradle:gradle это передача прав в Gradle
# . копировать все в папку (вместо точки можно выбирать только определенные папки) /home/gradle/src внутри контейнера
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
#должен удалить предыдущий джарник и собрать новый, скипает тесты
#если убрать команду ниже, то image будет собираться в разы быстрее,
#однако нужно будет самому делать пересборку джарника при каждом изменении
RUN gradle clean build -x test --no-daemon

## второй шаг, для запуска, причем используем только jre для запуска (так легче)
FROM openjdk:11.0.7-jre-slim
##необязательная команда, так как он все равно развернется на 80 порту учитывая запись в application.properties
EXPOSE 80
RUN mkdir /app
## копируем только джарник, так как он у нас появился только после первого шага build
COPY --from=GRADLE_BUILD /home/gradle/src/build/libs/evgen_spring_data-0.1.jar /app/spring-data.jar
# выполняет команду по запуску джарника
CMD ["java", "-jar", "/app/spring-data.jar"]
